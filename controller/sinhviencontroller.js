function domId(id) {
  return document.getElementById(id);
}

function layThongTinTuForm() {
  var ma = domId("txtMaSV").value;
  var ten = domId("txtTenSV").value;
  var email = domId("txtEmail").value;
  var hinhAnh = domId("txtHinhAnh").value;
  return {
    ma: ma,
    email: email,
    ten: ten,
    hinhAnh: hinhAnh,
  };
}

function showThongTinLenForm(dssv) {
  domId("txtMaSV").value = dssv.ma;
  domId("txtTenSV").value = dssv.ten;
  domId("txtEmail").value = dssv.email;
  domId("txtHinhAnh").value = dssv.hinhAnh;
}
