var dssv = []
function batLoading(){
    domId('loading').style.display = "flex"
}
function tatLoading(){
    domId('loading').style.display = "none"
}
var link = `https://62f8b754e0564480352bf3de.mockapi.io`
function renderTable(dssv){
    var hienThiTable = ``
    dssv.forEach(function(item){
        var hienThiHang = `<tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td><img src="${item.hinhAnh}" alt="" style="width:50px"/></td>
        <td>
        <button class="btn btn-success" onclick="xoaSv('${item.ma}')">Xóa</button>
        <button class="btn btn-success" onclick="suaSv('${item.ma}')">sửa</button>
        </td>
        </tr>`
        hienThiTable += hienThiHang
    })
    document.getElementById('tbodySinhVien').innerHTML = hienThiTable
}
// indssv
function renderTableSever(){
    batLoading()
    axios({
        url: `${link}/sv`,
        method:'GET'
    })
    .then(function(res){
        tatLoading()
        var dssv = res.data
        renderTable(dssv)
    })
    .catch(function(err){
        tatLoading()
        console.log(err)
    })
}
renderTableSever()
//them sv


function themSv(){
    batLoading()
    var dataForm = layThongTinTuForm()
    axios({
        url: `${link}/sv`,
        method:'POST',
        data: dataForm,
    })
    .then(function(res){
        tatLoading()
        renderTableSever()
    })
    .catch(function(err){
        tatLoading()
        console.log(err)
    })
}

// xoaSV
function xoaSv(id){
    batLoading()
    axios({
        url:`${link}/sv/${id}`,
        method: "DELETE",
    })
    .then(function(res){
        tatLoading()
        renderTableSever()
    })
    .catch(function(err){
        tatLoading()
        console.log("123")
    })
}

//SuaSV
function suaSv(id){
    batLoading()
    axios({
        url: `${link}/sv/${id}`,
        method: "GET",
    })
    .then(function(res){
        tatLoading()
       showThongTinLenForm(res.data)
    })
    .catch(function(err){
        tatLoading()
        console.log(err)
    })
}

// capnhat

function capNhatSv(){
    batLoading()
    var dataForm = layThongTinTuForm()
    console.log('dataForm: ', dataForm);
    axios({
        url:`${link}/sv/${dataForm.ma}`,
        method:`PUT`,
        data: dataForm,
    })
    .then(function(res){
        tatLoading()
        renderTableSever()
    domId('formQLSV').reset();

    })
    .catch(function(err){
        tatLoading()
        console.log(err)
    })
}

// reset
function resetForm(){
    domId('formQLSV').reset();
}